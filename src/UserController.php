<?php 

namespace App;


use Psr\Log\LoggerInterface;
use Illuminate\Database\Query\Builder;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use \mysqli;

class UserController
{
    protected $user;
    protected $app;
    protected $category;
    protected $enabled;
    protected $version;

    public function __construct(
    	Builder $user, Builder $app, Builder $category, Builder $enabled, Builder $version
    	) {
        $this->user = $user;
        $this->app = $app;
        $this->category = $category;
        $this->enabled = $enabled;
        $this->version = $version;
    }

    public function getUser (Request $request, Response $response, $args) {
		return $response->withJSON(['result' => $this->user->get()]);
	}

	public function getApp (Request $request, Response $response, $args) {
		$result = $this->app
			->join('category','category.id', '=', 'app.category_id')
			->get();
		return $response->withJSON(['result' => $result]);
	}

	public function getCategory (Request $request, Response $response, $args) {
		return $response->withJSON(['result' => $this->category->get()]);
	}

	public function getEnabled (Request $request, Response $response, $args) {
		return $response->withJSON(['result' => $this->enabled->get()]);
	}

	public function getVersion (Request $request, Response $response, $args) {
		return $response->withJSON(['result' => $this->version->get()]);
	}

}