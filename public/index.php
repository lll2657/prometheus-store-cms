<?php

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

// Instantiate the app
$settings = [
	'settings' => [
    'displayErrorDetails' => true, // set to false in production
    'addContentLengthHeader' => false, // Allow the web server to send the content-length header
	'determineRouteBeforeAppMiddleware' => false,
    'db' => [
        'driver' => 'mysql',
        'host' => 'localhost:8889',
        'database' => 'net_dragon',
        'username' => 'root',
        'password' => 'root',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
     ],

    // Renderer settings
    'renderer' => [
        'template_path' => __DIR__ . '/../templates/',
    ],

    // Monolog settings
    'logger' => [
        'name' => 'slim-app',
        'path' => __DIR__ . '/../logs/app.log',
    ],
   ],
];



$app = new \Slim\App($settings);

$container = $app->getContainer();

$container['db'] = function ($container) {
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($container->get('settings')['db']);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();
    return $capsule;
};

$container['\App\UserController'] = function ($c) {
    $user = $c->get('db')->table('user');
    $app = $c->get('db')->table('app');
    $category = $c->get('db')->table('category');
    $enabled = $c->get('db')->table('enabled');
    $version = $c->get('db')->table('version');
    return new \App\UserController($user, $app, $category, $enabled, $version);
};


$app->get('/users', '\App\UserController:getUser');	

$app->get('/apps', '\App\UserController:getApp');	

// $app->get('/apps_eng', function ($request, $response, $args) {

// 	$servername = "localhost:8889";
// 	$username = "root";
// 	$password = "root";
// 	$dbname = "net_dragon";

// 	// Create connection
// 	$conn = new mysqli($servername, $username, $password, $dbname);

// 	// Check connection
// 	if ($conn->connect_error) {
// 	    die("Connection failed: " . $conn->connect_error);
// 	} 
	
// 	$sql = "SELECT *, app.title as app_title, category.title as category_title FROM app FULL OUTER JOIN category ON app.id = category.id";
// 	$result = $conn->query($sql);

// 	if ($result->num_rows > 0) {
//     	 // output data of each row
//     	 while($row = $result->fetch_assoc()) {
//         	 return $response->withJSON(['result' => [$row]]);
//      	}
// 	} else {
//      return $response->withJSON(['result' => 'no results']);
// 	}

// 	$conn->close();

// });	

// $app->get('/categories_tc', function ($request, $response, $args) {

// 	$servername = "localhost:8889";
// 	$username = "root";
// 	$password = "root";
// 	$dbname = "net_dragon";

// 	// Create connection
// 	$conn = new mysqli($servername, $username, $password, $dbname);

// 	// Check connection
// 	if ($conn->connect_error) {
// 	    die("Connection failed: " . $conn->connect_error);
// 	} 
	
// 	$sql = "SELECT * FROM category FULL OUTER JOIN translation ON translation.table_name = 'category' AND translation.lang_id = 'tc' WHERE translation.table_id = category.id";
// 	$result = $conn->query($sql);

// 	if ($result->num_rows > 0) {
//     	 // output data of each row
//     	 while($row = $result->fetch_assoc()) {
//         	 return $response->withJSON(['result' => [$row]]);
//      	}
// 	} else {
//      return $response->withJSON(['result' => 'no results']);
// 	}

// 	$conn->close();

// });	

// Run app
// INSERT INTO `net_dragon`.`app` (`id`, `title_en`, `title_tc`, `title_sc`, `img_path`, `sort_num`, `description_en`, `description_tc`, `description_sc`, `ios_list_info_en`, `ios_list_info_tc`, `ios_list_info_sc`, `aos_list_info_en`, `aos_list_info_tc`, `aos_list_info_sc`, `bundle_id`, `created_at`, `updated_at`, `modifier_id`, `category_id`) VALUES ('1', 'example', '範例', '范例', '', '1', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'root', '1');
$app->run();
